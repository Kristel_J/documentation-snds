# Ressources
<!-- SPDX-License-Identifier: MPL-2.0 -->

## Documents partagés sur cette documentation
- [KWIKLY - Katalogue SNIIRAM SNDS](kwikly.md) [Cnam - MPL-2.0] permet de connaitre la présence des variables dans l’historique des produits depuis 2006.
- [Historique des données SNDS](/assets/src/2019_INDS_Historique-des-données-SNDS.pptx) [INDS - MPL-2.0]
- [Dictionnaire DCIR et PMSI/MCO](/assets/src/2019-04_GIS_EPI-PHARE_DICO_DCIR_4_Vue_MPL-2.0.xlsx) [GIS EPI-PHARE - MPL-2.0] utilisé en interne pour reconstruire des vues SAS. Contient notamment les années de disponibilité des tables et variables.
- [DCIR-Formats.zip](/assets/src/2019-04_GIS_EPI-PHARE_DCIR-Formats_MPL-2.0.zip) [GIS EPI-PHARE - MPL-2.0]  
Archive comprenant
  - `PI_DCIR_Formats.docx`, un manuel sur les vues et formats DCIR
  - 33  fichiers xlsx retraçant les évolutions mensuelles de certaines tables de valeurs

## Autres ressources disponibles en ligne

- Le nouveau [forum d'entraide](https://entraide.health-data-hub.fr) de la communauté des utilisateurs du SNDS.

- Le site [snds.gouv](https://www.snds.gouv.fr/SNDS/Accueil) 
pour des informations générales.

- Le [Wiki SNIIRAM](http://open-data-assurance-maladie.ameli.fr/wiki-sniiram/index.php/Accueil_-_Dictionnaire_de_donn%C3%A9es_SNIIRAM) 
et notamment sa [FAQ](http://open-data-assurance-maladie.ameli.fr/wiki-sniiram/index.php/Questions-R%C3%A9ponses),
édité par la Cnam.

- Le [site de l'ATIH](https://www.atih.sante.fr)
pour des détails sur le PMSI
    - partie [information médicale](https://www.atih.sante.fr/domaines-d-activites/information-medicale) du site
    - plateforme de restitution des données des établissements de santé [scansanté](https://www.scansante.fr) 

- Le [site du CépiDc](https://cepidc.inserm.fr/causes-medicales-de-deces/la-base-des-causes-medicales-de-deces)
pour des détails sur la base médicale des causes de décès.

- Un [dictionnaire interactif](http://dico-snds.health-data-hub.fr/) du SNDS, produit par la DREES.

- Un [schema formalisé du SNDS](https://gitlab.com/healthdatahub/schema-snds), 
qui alimente le dictionnaire interactif, et la partie [table](/tables) de cette documentation.

- Le site open data du gouvernement : [data.gouv](https://www.data.gouv.fr) et la [partie dédiée à la santé](https://www.data.gouv.fr/fr/topics/sante-et-social/) avec notamment sur le profil de [l'assurance maladie](https://www.data.gouv.fr/fr/organizations/caisse-nationale-de-l-assurance-maladie-des-travailleurs-salaries/) qui répértorie tous les jeux de données open data de la Cnam.  


# Présentations 

## 2019.03.27 Meetup-SNDS1

[Page de l'événement](https://www.meetup.com/fr-FR/Health-Data-Hub/events/259764548/)

- Documentation ouverte du SNDS sur GitLab ([documentation-snds.health-data-hub.fr](https://documentation-snds.health-data-hub.fr/))
    - Présenté par Anne Cuerq de l'INDS et Pierre-Alain Jachiet de la DREES
    - Support de présentation
    [GSlides](https://docs.google.com/presentation/d/1WvC4879Exta3Iv5vT9p-aHdzEjl4vy4ElymcNYWDXCQ/edit#slide=id.g54f4ce99ab_0_5),
    [pdf](/assets/src/presentations/meetup-snds1/2019.03.27_INDS_DREES_Documentation_SNDS_MPL-2.0.pdf),
    [enregistement](https://www.youtube.com/watch?v=JpUbZUWwEt4)
    
- Schéma formel et Dictionnaire interactif du SNDS ([dico-snds.health-data-hub.fr](http://dico-snds.health-data-hub.fr/))
    - Présenté par Matthieu Doutreligne et Vitkor Jarry de la DREES
    - Support de présentation
    [GSlides](https://docs.google.com/presentation/d/1XMGUJKcxy1zqfII-EtQDbmR2tljj6A8NIyEiEKJmz-Q/edit#slide=id.g54533cc6ad_0_0), 
    [pdf](/assets/src/presentations/meetup-snds1/2019.03.27_DREES_schema_dico_snds_MPL-2.0.pdf),
    [enregistement](https://www.youtube.com/watch?v=aAeAB301zzM)

- Appariements entre les cohortes et le SNDS - zoom sur un cas concret
    - Présenté par Mehdi Gabbas et Brice Dufresne de la CNAM
    - Support de présentation
     [pptx](/assets/src/presentations/meetup-snds1/2019.03.27_CNAM_Possibilités_Appariement_SNDS_MPL-2.0.pptx) ,
    [pdf](/assets/src/presentations/meetup-snds1/2019.03.27_CNAM_Possibilités_Appariement_SNDS_MPL-2.0.pdf),
    [enregistement](https://www.youtube.com/watch?v=8M12owyEst4)
