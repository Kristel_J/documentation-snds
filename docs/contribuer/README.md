# Guide de contribution
<!-- SPDX-License-Identifier: MPL-2.0 -->

Bienvenue sur le guide de contribution à la documentation collaborative !

Cette page présente les grands types de contributions que vous pouvez apporter, et comment procéder. 
Des explications pratiques sont disponibles dans les pages suivantes.

## Quelles contributions ?

### Partage de documents existants

De nombreux organismes ont documenté le SNDS pour des besoins internes. 
Ce travail n'est souvent pas accessibles à la communauté plus large des utilisateurs du SNDS. 
Une première contribution de grande valeur consiste à partager publiquement des documents existants.

**Où publier ces documents ?**

- sur le site internet de votre organisation. 
Nous les référencerons via un lien hypertexte dans la section des 
[ressources externes](/documentation/ressources#autres-ressources-disponibles-en-ligne). 

- sur ce site internet. 
Nous ajouterons alors un lien de téléchargement dans la section des 
[documents partagés via ce projet](/documentation/ressources#documents-partages-sur-cette-documentation).


::: tip Note
Le nom de l'organisation sera indiqué dans la description du document, et dans le nom du fichier.

Une **licence MPL-2.0** est appliquée sur tous les fichiers partagés via ce site. 

Cette licence [copyleft](https://fr.wikipedia.org/wiki/Copyleft) 
autorise une libre réutilisation du fichier ainsi partagé.
Elle impose à toute version dérivée de citer la source d'origine, 
et d'être publiée sous la même licence afin de garantir les mêmes droits. 
:::

### Amélioration des pages de documentation collaborative 

Les contributions visant à corriger des erreurs ou à compléter des pages existantes sont très précieuses. 
Toute amélioration, même d'apparence mineure comme la correction de fautes d'orthographe, améliorera pour tous la qualité du contenu.

### Création de nouvelles pages du site internet

La documentation du SNDS est éclatée dans de multiples fichiers, sous des formats hétérogènes : pdf, word, excel, powerpoint, etc. 
Cette disparité complexifie l'exploration des informations, 
et rend surtout impossible une collaboration large sur l'amélioration du contenu. 

Ce site internet est rédigé en texte brut. 
Une excellente contribution consiste à créer de nouvelles pages de documentation, 
à partir de vos connaissances ou à partir du contenu de fichiers déjà partagés par ailleurs.
 
Nous organiserons progressivement les pages ainsi crées selon des thématiques cohérentes.   

## Comment contribuer ?

Cette section présente les principaux canaux de contribution.

### Email

Le plus simple est d'écrire un email aux mainteneurs du projets :
- Lab santé - DREES <<ld-lab-github@sante.gouv.fr>>
- Olivier de Fresnoye - INDS <<olivier.defresnoye@indsante.fr>>

Nous nous chargerons alors d'intégrer votre contribution.

### Issues

En créant un compte sur gitlab.com, vous pourrez utiliser le système de tickets appelés **issues**, dans cet 
[onglet](https://gitlab.com/healthdatahub/documentation-snds/issues). 

Les issues sont le canal à privilégier pour remonter une erreur ou proposer une idée.  
Chaque issue est l'occasion d'une discussion ouverte pour résoudre le problème évoqué.

### Merge-Request

GitLab permet à chacun de proposer des modifications via des **merge-request**, listées dans cet 
[onglet](https://gitlab.com/healthdatahub/documentation-snds/merge_requests). 

Une formation est préférable pour manipuler facilement `git`, `GitLab` et les `merge-request`.

Les mainteneurs du projet organisent de telles formations (gratuites). N'hésitez pas à nous solliciter ! 

- Pour les autodidactes, la page suivante propose une [introdution à GitLab](introduction_gitlab.md).
- Pour les développeurs, voici un mémo pour [éditer le site en local](developpement_local.md).
